#pragma once

#ifndef _PARTICLE_H_
#define _PARTICLE_H_

#include "State.h"

class Particle {
private:
	State state;
	glm::vec3 externalForce;
	glm::vec3 previousForce;
	bool fixed;

public:
	Particle() : state(glm::vec3(0), glm::vec3(0), 1.0f), fixed(false), externalForce(0), previousForce(0) {}
	explicit Particle(glm::vec3 position) : state(position, glm::vec3(0), 1.0f), 
		fixed(false), externalForce(0), previousForce(0) {}

	glm::vec3 getPos() const;
	void setPos(glm::vec3 pos);

	glm::vec3 getVel() const;

	bool isFixed() const;
	void setFixed(bool fixed);

	void addExternalForce(glm::vec3 force);
	glm::vec3 getPreviousForce() const;

	void update(float dt);
};

#endif