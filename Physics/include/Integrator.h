#pragma once

#ifndef _INTEGRATOR_H_
#define _INTEGRATOR_H_

#include <glm/glm.hpp>

#include "State.h"

Derivative evaluate(State const& state, float dt, Derivative const& d, glm::vec3 force);
State rk4Integrator(State const& state, float dt, glm::vec3 force);

#endif