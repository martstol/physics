#pragma once

#ifndef _PARTICLE_SYSTEM_H_
#define _PARTICLE_SYSTEM_H_

#include "Particle.h"
#include "Prng.h"
#include "Shader.h"
#include "Camera.h"

#include <GL/glew.h>

#include <vector>

class ParticleSystem {
private:
	std::vector<Particle> particles;
	GLuint vertexBufferObject;
	GLuint positionBufferObject;
	ShaderProgram shader;

public:
	explicit ParticleSystem(FloatPrng & prng, size_t numParticles);
	~ParticleSystem();

	ParticleSystem(ParticleSystem & other) = delete;
	ParticleSystem operator=(ParticleSystem & other) = delete;

	ParticleSystem(ParticleSystem && other);
	ParticleSystem& operator=(ParticleSystem && other);

	void update(float dt);
	void render(Camera const& camera) const;
	size_t numParticles() const;
	Particle& operator[](size_t i);
	Particle const& operator[](size_t i) const;
	std::vector<Particle>::iterator begin();
	std::vector<Particle>::iterator end();
	std::vector<Particle>::const_iterator begin() const;
	std::vector<Particle>::const_iterator end() const;
};

#endif