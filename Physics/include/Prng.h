#pragma once

#ifndef _PRNG_H_
#define _PRNG_H_

#include <random>

template <class Gen, class Dist>
class Prng {
private:
	Gen gen;
	Dist dist;

public:
	explicit Prng(typename Gen::result_type seed, typename Dist::result_type low, typename Dist::result_type high) : gen(seed), dist(low, high) {}

	typename Dist::result_type next() { return dist(gen); }
};

typedef Prng<std::mt19937, std::uniform_int_distribution<int>> IntPrng;
typedef Prng<std::mt19937, std::uniform_real_distribution<float>> FloatPrng;

#endif