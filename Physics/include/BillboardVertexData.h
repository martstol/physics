#pragma once

#ifndef _BILLBOARDVERTEXDATA_H_
#define _BILLBOARDVERTEXDATA_H_

#include <vector>

#include <glm/glm.hpp>

static std::vector<glm::vec3> const billboardVertices = {
	glm::vec3{-1.0f, -1.0f,  0.0f},
	glm::vec3{ 1.0f, -1.0f,  0.0f},
	glm::vec3{-1.0f,  1.0f,  0.0f},
	glm::vec3{ 1.0f,  1.0f,  0.0f},
};

#endif