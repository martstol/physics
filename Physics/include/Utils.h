#pragma once

#ifndef _UTILS_H_
#define _UTILS_H_

#include <string>

std::string readFile(std::string const& filename);

#endif