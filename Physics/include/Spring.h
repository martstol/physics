#pragma once

#ifndef _SPRING_H_
#define _SPRING_H_

#include "ParticleSystem.h"

#include <vector>

class Spring {
private:
	float k; // Spring constant
	float d; // Spring length
	int i, j; // Particle indices

public:
	explicit Spring(int i, int j, float k = 5.0f, float d = 0.5f) : k(k), d(d), i(i), j(j) {}

	void applyForce(ParticleSystem & particleSystem);

};

#endif