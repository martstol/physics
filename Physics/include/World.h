#pragma once

#ifndef _WORLD_H_
#define _WORLD_H_

#include "ParticleSystem.h"

#include <glm/glm.hpp>

class World {
private:
	glm::vec3 gravity;
	float friction;

public:
	World(glm::vec3 gravity = { 0.0f, -9.81f, 0.0f }, float friction = 1.0f) : gravity(gravity), 
		friction(friction) {}
	
	void applyForce(ParticleSystem & particleSystem);

};

#endif