#pragma once

#ifndef _STATE_H_
#define _STATE_H_

#include <glm/glm.hpp>

#include <cassert>

class State {
private:
	glm::vec3 position;
	glm::vec3 momentum;
	float mass;

public:
	explicit State(glm::vec3 position, glm::vec3 momentum, float mass) : 
		position(position), momentum(momentum), mass(mass) { assert(mass > 0.0f); }

	glm::vec3 getPosition() const { return position; }
	glm::vec3 getMomentum() const { return momentum; }
	float getMass() const { return mass; }
};

class Derivative {
private:
	glm::vec3 dPosition;
	glm::vec3 dMomentum;

public:
	Derivative() : dPosition(0), dMomentum(0) {}
	explicit Derivative(glm::vec3 dx, glm::vec3 dp) : dPosition(dx), dMomentum(dp) {}

	glm::vec3 getDPosition() const { return dPosition; }
	glm::vec3 getDMomentum() const { return dMomentum; }
};

#endif