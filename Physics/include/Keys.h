#pragma once

#ifndef _KEYS_H_
#define _KEYS_H_

#include <SFML/Window/Mouse.hpp>
#include <glm/glm.hpp>

class Key {
private:
	bool pressed;
public:
	Key() : pressed(false) {}
	void press() { pressed = true; }
	void release() { pressed = false; }
	void setPressed(bool p) { pressed = p; }
	bool isPressed() const { return pressed; }
};

class Mouse {
public:
	Key left;
	Key right;
	glm::vec2 pos;
	glm::vec2 delta;
	Mouse() : pos(glm::vec2{sf::Mouse::getPosition().x, sf::Mouse::getPosition().y}), delta(glm::vec2{ 0 }) {}
};

class Keys {
public:
	Key up;
	Key down;
	Key left;
	Key right;
	Key space;
	Key ctrl;
	Key shift;
	Mouse mouse;
};

#endif