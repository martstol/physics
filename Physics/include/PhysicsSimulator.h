#pragma once

#ifndef _PHYSICS_SIMULATOR_H_
#define _PHYSICS_SIMULATOR_H_

#include "Spring.h"
#include "World.h"
#include "ParticleSystem.h"
#include "Prng.h"
#include "Camera.h"
#include "Keys.h"

#include <SFML/Window.hpp>

#include <vector>

class PhysicsSimulator {
private:
	FloatPrng prng;
	float const dt = 1 / 60.f;
	float t = 0.f;
	Camera camera;
	Keys keys;
	bool running;

	ParticleSystem particleSystem;
	std::vector<Spring> springs;
	World world;

public:
	explicit PhysicsSimulator(sf::Vector2u windowSize, size_t numParticles, int seed);
	void update();
	void render() const;
	void handleEvent(sf::Event const& event);
	void handleKeyEvent(sf::Event const& event);
	void handleMousePressEvent(sf::Event const& event);
	void handleMouseMoveEvent(sf::Event const& event);
	bool isRunning() const;

};

#endif