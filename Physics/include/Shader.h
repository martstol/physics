#pragma once

#ifndef _SHADER_H_
#define _SHADER_H_

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <string>

class ShaderProgram {
private:
	GLuint programId;

public:
	ShaderProgram(std::string const& vertFilename, std::string const& fragFilename);
	~ShaderProgram();

	ShaderProgram(ShaderProgram & other) = delete;
	ShaderProgram & operator=(ShaderProgram & other) = delete;

	ShaderProgram(ShaderProgram && other);
	ShaderProgram & operator=(ShaderProgram && other);

	void bind() const;

	static void setUniformValue(GLint location, glm::mat4 const& mat) {
		glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(mat));
	}

	static void setUniformValue(GLint location, glm::vec3 const& vec) {
		glUniform3fv(location, 1, glm::value_ptr(vec));
	}

	static void setUniformValue(GLint location, GLint value) {
		glUniform1i(location, value);
	}
};

#endif