#version 450 core

layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec3 particlePos;

out vec2 uv;

layout(location = 0) uniform mat4 vp;

void main() {
	vec3 pos = particlePos + vertexPos * 0.1;

	gl_Position = vp * vec4(pos, 1);
	uv = 0.5 * vertexPos.xy + vec2(0.5);
}
