#include "World.h"

#include <algorithm>

void World::applyForce(ParticleSystem & particleSystem) {
	std::for_each(particleSystem.begin(), particleSystem.end(),
		[this](Particle & p) {
		p.addExternalForce(gravity); 
		p.addExternalForce(-p.getVel()*friction);
	});
}
