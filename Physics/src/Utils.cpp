#include "Utils.h"

#include <fstream>
#include <sstream>
#include <exception>

std::string readFile(std::string const& filename) {
	std::ifstream in(filename);
	if (in.fail()) {
		throw std::runtime_error("Could not open file: " + filename);
	}
	std::ostringstream ss;
	ss << in.rdbuf();
	return ss.str();
}