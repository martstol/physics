#include "Particle.h"
#include "Integrator.h"

#include <iostream>

void Particle::update(float dt) {
	if (!fixed) {
		state = rk4Integrator(state, dt, externalForce);
		previousForce = externalForce;
		externalForce = glm::vec3{ 0 };
	}
}

glm::vec3 Particle::getPos() const {
	return state.getPosition();
}

void Particle::setPos(glm::vec3 pos) {
	state = State{pos, state.getMomentum(), state.getMass()};
}

glm::vec3 Particle::getVel() const {
	return state.getMomentum() / state.getMass();
}

bool Particle::isFixed() const {
	return fixed;
}

void Particle::setFixed(bool fixed) {
	this->fixed = fixed;
}

void Particle::addExternalForce(glm::vec3 force) {
	externalForce += force;
}

glm::vec3 Particle::getPreviousForce() const {
	return previousForce;
}
