#include "PhysicsSimulator.h"

#include <SFML/Window.hpp>
#include <GL/glew.h>

#include <iostream>
#include <exception>
#include <random>

void setupGL() {
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		throw std::runtime_error("Initialize glew failed");
	}
	glClearColor(.95f, .95f, .95f, .0f);
	glEnable(GL_DEPTH_TEST);
}

void printGLInfo() {
	std::printf("OpenGL Context Information:\n");

	int versionMajor, versionMinor;
	glGetIntegerv(GL_MAJOR_VERSION, &versionMajor);
	glGetIntegerv(GL_MINOR_VERSION, &versionMinor);
	std::printf("GL Version:\t %d.%d\n", versionMajor, versionMinor);

	std::printf("GL Vendor:\t %s\n", glGetString(GL_VENDOR));

	std::printf("GL Renderer:\t %s\n", glGetString(GL_RENDERER));

	std::printf("GLSL Version:\t %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
}

int main() {
	try {
		sf::ContextSettings settings;
		settings.antialiasingLevel = 4;

		sf::Window window(sf::VideoMode(800, 600), "Physics", sf::Style::Default, settings);
		window.setFramerateLimit(60);

		setupGL();
		printGLInfo();

		std::random_device rd;
		PhysicsSimulator simulator(window.getSize(), 10, rd());

		while (simulator.isRunning()) {
			simulator.update();
			simulator.render();
			window.display();

			sf::Event event;
			while (window.pollEvent(event)) {
				simulator.handleEvent(event);
			}
		}
		window.close();

	} catch (std::runtime_error &ex) {
		std::cout << ex.what() << "\n";
	} catch (...) {
		std::cout << "An unexpected exception occurred\n";
	}
}