#include "Shader.h"
#include "Utils.h"

#include <vector>
#include <functional>

void errorCheck(GLuint id, GLenum parameter, std::string const& name,
	std::function<void(GLuint, GLenum, GLint*)> parmFunc,
	std::function<void(GLuint, GLsizei, GLsizei*, GLchar*)> logFunc) {

	GLint status = 0;
	parmFunc(id, parameter, &status);
	if (status == GL_FALSE) {
		int logLength;
		parmFunc(id, GL_INFO_LOG_LENGTH, &logLength);

		std::vector<char> log(logLength);
		logFunc(id, logLength, nullptr, log.data());

		throw std::runtime_error("Error (" + name + "): " + std::string(log.begin(), log.end()));
	}
}

GLuint loadShader(GLenum type, std::string const& filename) {
	GLuint shaderId = glCreateShader(type);

	std::string shaderCode = readFile(filename);
	GLint length = shaderCode.length();
	GLchar const * data = shaderCode.c_str();

	glShaderSource(shaderId, 1, &data, &length);
	glCompileShader(shaderId);

	errorCheck(shaderId, GL_COMPILE_STATUS, filename, glGetShaderiv, glGetShaderInfoLog);

	return shaderId;
}

GLuint linkProgram(GLuint vertexShaderId, GLuint fragmentShaderId) {
	GLuint programId = glCreateProgram();

	glAttachShader(programId, vertexShaderId);
	glAttachShader(programId, fragmentShaderId);
	glLinkProgram(programId);

	errorCheck(programId, GL_LINK_STATUS, "linking", glGetProgramiv, glGetProgramInfoLog);

	return programId;
}

ShaderProgram::ShaderProgram(std::string const& vertFilename, std::string const& fragFilename) {
	GLuint vertexShaderId = loadShader(GL_VERTEX_SHADER, vertFilename);
	GLuint fragmentShaderId = loadShader(GL_FRAGMENT_SHADER, fragFilename);

	programId = linkProgram(vertexShaderId, fragmentShaderId);

	glDeleteShader(vertexShaderId);
	glDeleteShader(fragmentShaderId);
}

ShaderProgram::~ShaderProgram() {
	glDeleteProgram(programId);
}

ShaderProgram::ShaderProgram(ShaderProgram && other) {
	programId = other.programId; other.programId = 0;
};

ShaderProgram & ShaderProgram::operator=(ShaderProgram && other) {
	glDeleteProgram(programId);
	programId = other.programId; other.programId = 0;
	return *this;
};

void ShaderProgram::bind() const {
	glUseProgram(programId);
}
