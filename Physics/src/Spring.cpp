#include "Spring.h"

#include <iostream>

void Spring::applyForce(ParticleSystem & particleSystem) {
	Particle & p1 = particleSystem[i];
	Particle & p2 = particleSystem[j];

	glm::vec3 x = p2.getPos() - p1.getPos();
	float l = glm::length(x);

	glm::vec3 f = -k * (l - d) * (x / l);

	p2.addExternalForce(f);
	p1.addExternalForce(-f);
}