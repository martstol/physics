#include "ParticleSystem.h"
#include "BillboardVertexData.h"

#include <algorithm>
#include <utility>
#include <iostream>

void ParticleSystem::update(float dt) {
	std::for_each(particles.begin(), particles.end(),
		[dt](Particle & particle) {particle.update(dt); });
}

void ParticleSystem::render(Camera const& camera) const {
	std::vector<glm::vec3> positions(particles.size());
	std::transform(particles.begin(), particles.end(), positions.begin(),
		[](Particle const& particle) {return particle.getPos(); });

	std::stable_sort(positions.begin(), positions.end(),
		[&camera](glm::vec3 const& a, glm::vec3 const& b) {
		float i = a.z - camera.getPosition().z;
		float j = b.z - camera.getPosition().z;
		return i*i > j*j;
	});

	glBindBuffer(GL_ARRAY_BUFFER, positionBufferObject);
	glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(glm::vec3), nullptr, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, positions.size() * sizeof(glm::vec3), positions.data());

	shader.bind();
	glm::mat4 vp = camera.getViewProjMatrix();
	ShaderProgram::setUniformValue(0, vp);
	glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, particles.size());
}

ParticleSystem::ParticleSystem(FloatPrng & prng, size_t numParticles) : particles(numParticles),
	shader("res/particle.vert", "res/particle.frag") {
	glGenBuffers(1, &vertexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*billboardVertices.size(), billboardVertices.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glVertexAttribDivisor(0, 0);

	glGenBuffers(1, &positionBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, positionBufferObject);
	glBufferData(GL_ARRAY_BUFFER, numParticles * sizeof(glm::vec3), nullptr, GL_STREAM_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glVertexAttribDivisor(1, 1);

	std::generate(particles.begin(), particles.end(),
		[&prng]() { return Particle{ glm::vec3{ prng.next(), prng.next(), prng.next()+5 } }; });
}

ParticleSystem::~ParticleSystem() {
	glDeleteBuffers(1, &vertexBufferObject);
	glDeleteBuffers(1, &positionBufferObject);
}

ParticleSystem::ParticleSystem(ParticleSystem && other) : shader(std::move(other.shader)) {
	particles = std::move(other.particles);
	vertexBufferObject = other.vertexBufferObject; other.vertexBufferObject = 0;
	positionBufferObject = other.positionBufferObject; other.positionBufferObject = 0;
}

ParticleSystem& ParticleSystem::operator=(ParticleSystem && other) {
	glDeleteBuffers(1, &vertexBufferObject);
	glDeleteBuffers(1, &positionBufferObject);
	shader = std::move(other.shader);
	particles = std::move(other.particles);
	vertexBufferObject = other.vertexBufferObject; other.vertexBufferObject = 0;
	positionBufferObject = other.positionBufferObject; other.positionBufferObject = 0;
	return *this;
}

size_t ParticleSystem::numParticles() const {
	return particles.size();
}

Particle& ParticleSystem::operator[](size_t i) {
	return particles[i];
}

Particle const& ParticleSystem::operator[](size_t i) const {
	return particles[i];
}

std::vector<Particle>::iterator ParticleSystem::begin() {
	return particles.begin();
}

std::vector<Particle>::iterator ParticleSystem::end() {
	return particles.end();
}

std::vector<Particle>::const_iterator ParticleSystem::begin() const {
	return particles.begin();
}

std::vector<Particle>::const_iterator ParticleSystem::end() const {
	return particles.end();
}
