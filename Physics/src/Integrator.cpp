#include "Integrator.h"

Derivative evaluate(State const& state, float dt, Derivative const& d, glm::vec3 force) {
	glm::vec3 momentum = state.getMomentum() + d.getDMomentum() * dt;
	return Derivative{momentum/state.getMass(), force};
}

State rk4Integrator(State const& state, float dt, glm::vec3 force) {
	Derivative a = evaluate(state, 0.0f, Derivative(), force);
	Derivative b = evaluate(state, dt*0.5f, a, force);
	Derivative c = evaluate(state, dt*0.5f, b, force);
	Derivative d = evaluate(state, dt, c, force);

	glm::vec3 dxdt = (1.0f / 6.0f) * (a.getDPosition() + 2.0f * (b.getDPosition() + c.getDPosition()) + d.getDPosition());
	glm::vec3 dpdt = (1.0f / 6.0f) * (a.getDMomentum() + 2.0f * (b.getDMomentum() + c.getDMomentum()) + d.getDMomentum());

	glm::vec3 position = state.getPosition() + dxdt * dt;
	glm::vec3 momentum = state.getMomentum() + dpdt * dt;

	return State{position, momentum, state.getMass()};
}