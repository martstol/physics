#include "PhysicsSimulator.h"

#include <GL/glew.h>

#include <algorithm>

PhysicsSimulator::PhysicsSimulator(sf::Vector2u windowSize, size_t numParticles, int seed)
	: particleSystem(prng, numParticles), prng(seed, 0.f, 1.f), running(true), camera(windowSize.x, windowSize.y) {
	for (size_t i = 0; i < numParticles/2; i++) {
		Spring spring(i, i + numParticles/2);
		particleSystem[i].setFixed(true);
		springs.push_back(spring);
	}
	for (size_t i = numParticles/2; i < numParticles; i++) {
		float dx = prng.next() - 0.5f;
		float dz = prng.next() - 0.5f;
		particleSystem[i].addExternalForce({100*dx, 1000, 100*dz});
	}
}

void PhysicsSimulator::update() {
	int dx = keys.right.isPressed() - keys.left.isPressed();
	int dy = keys.space.isPressed() - keys.ctrl.isPressed();
	int dz = keys.up.isPressed() - keys.down.isPressed();
	camera.right(dx*0.02f);
	camera.upward(dy*0.02f);
	camera.forward(dz*0.02f);

	glm::vec2 turn = keys.mouse.delta * 0.005f * static_cast<float>(keys.mouse.right.isPressed());
	camera.turn(-turn.x, turn.y);
	keys.mouse.delta = glm::vec2{ 0 };

	std::for_each(springs.begin(), springs.end(),
		[this](Spring & spring) {spring.applyForce(particleSystem);});
	world.applyForce(particleSystem);
	particleSystem.update(dt);
	t += dt;
}

void PhysicsSimulator::render() const {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	particleSystem.render(camera);
}

void PhysicsSimulator::handleEvent(sf::Event const& event) {
	switch (event.type) {
	case sf::Event::Closed:
		running = false;
		break;
	case sf::Event::Resized:
		camera.setScreenSize(event.size.width, event.size.height);
		glViewport(0, 0, event.size.width, event.size.height);
		break;
	case sf::Event::KeyPressed:
	case sf::Event::KeyReleased:
		handleKeyEvent(event);
		break;
	case sf::Event::MouseButtonPressed:
	case sf::Event::MouseButtonReleased:
		handleMousePressEvent(event);
		break;
	case sf::Event::MouseMoved:
		handleMouseMoveEvent(event);
		break;
	default:
		break;
	}
}

void PhysicsSimulator::handleKeyEvent(sf::Event const& event) {
	bool pressed = event.type == sf::Event::KeyPressed;
	switch (event.key.code) {
	case sf::Keyboard::W:
		keys.up.setPressed(pressed);
		break;
	case sf::Keyboard::S:
		keys.down.setPressed(pressed);
		break;
	case sf::Keyboard::A:
		keys.left.setPressed(pressed);
		break;
	case sf::Keyboard::D:
		keys.right.setPressed(pressed);
		break;
	case sf::Keyboard::Space:
		keys.space.setPressed(pressed);
		break;
	case sf::Keyboard::LControl:
		keys.ctrl.setPressed(pressed);
	default:
		break;
	}
}

void PhysicsSimulator::handleMousePressEvent(sf::Event const& event) {
	bool pressed = event.type == sf::Event::MouseButtonPressed;
	switch (event.mouseButton.button) {
	case sf::Mouse::Left:
		keys.mouse.left.setPressed(pressed);
		break;
	case sf::Mouse::Right:
		keys.mouse.right.setPressed(pressed);
		break;
	default:
		break;
	}
}

void PhysicsSimulator::handleMouseMoveEvent(sf::Event const& event) {
	glm::vec2 pos{ event.mouseMove.x, event.mouseMove.y };
	keys.mouse.delta += pos - keys.mouse.pos;
	keys.mouse.pos = pos;
}

bool PhysicsSimulator::isRunning() const {
	return running;
}
